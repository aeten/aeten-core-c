#include "Iterable.h"

/*!
@startuml
!include Iterator.c
namespace aneth {
    interface Iterable<T extends Object> {
        + Iterator<T> iterator()
	}
}
@enduml
*/

