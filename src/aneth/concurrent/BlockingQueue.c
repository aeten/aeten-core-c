#include "BlockingQueue.h"

/*!
@startuml
!include aneth/Queue.c
namespace aneth.concurrent {
    interface BlockingQueue<T extends Object> extends aneth.Queue {
        + void put(T element)
        + T take()
	}
}
@enduml
*/
