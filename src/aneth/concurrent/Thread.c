#include "Thread.h"

/*!
@startuml
namespace aneth.concurrent {
	interface Thread<T> {
		+ start()
		+ stop()
		+ T* join()
	}
}
@enduml
*/

