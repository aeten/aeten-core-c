#include "Map.h"

/*
@startuml(id=Map)
!include Collection.c
!include Set.c
namespace aneth {
    interface Map<K extends Object,V extends Object> {
        + V get(K key)
        + V put(K key, V value) <<default>> <<optional>>
        + V remove(K key) <<default>> <<optional>>
		+ size_t size()
        + Collection<V> values()
        + Set<MapEntry<K,V>> entrySet()
        + Set<K> keySet()
	}
!ifdef aneth_Map
	note right of Map::get
		Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
	end note
	note right of Map::put
		Associates the specified value with the specified key in this map (optional operation).
	end note
	note right of Map::remove
		Removes the mapping for a key from this map if it is present (optional operation).
	end note
!endif
}
@enduml
*/

aneth_Object Map_put(Map* map, aneth_Object key, aneth_Object value) {
	check(0, UnsupportedOperationException, "Map.put()");
	return (aneth_Object){0};
}

aneth_Object Map_remove(Map* map, aneth_Object key) {
	check(0, UnsupportedOperationException, "Map.remove()");
	return (aneth_Object){0};
}
