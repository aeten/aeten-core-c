all: test
library:
	ninja deps
	ninja library
check: library
	ninja test
clean:
	ninja clean

.PHONY = all checkclean library
